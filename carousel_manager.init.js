/**
 * @file
 * Provides Slick loader.
 */

/* global jQuery:false, Drupal:false */
/* jshint -W072 */
/* eslint max-params: 0, consistent-this: [0, "_"] */
(function ($, Drupal) {

  "use strict";

  Drupal.behaviors.carousel_manager = {
    attach: function (context, settings) {
      var php = settings.carousel_manager;

      php.slides.shift();
      $(document).ready(function(){
        console.log(php.slides);
        for (var i = 0, len = php.slides.length; i < len; i++) {
          console.log(php.slides[i]['slide']);
          $('#' + php.delta).slick(
            'slickAdd', '\
            <div class="slick__slide">\
              <div class="slide-background">\
                ' + php.slides[i]['slide'] + '\
              </div>\
            </div>'
          );
        }
      });
    }
  }
})(jQuery, Drupal);
