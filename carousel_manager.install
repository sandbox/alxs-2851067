<?php

/**
 * @file
 * Create tables for entity and entity bundles.
 */

/**
 * Implements hook_schema().
 */
function carousel_manager_schema() {
  $schema = array();

  // Table for storing data of entities.
  $schema['carousel_manager'] = array(
    'description' => 'The base table for carousel manager slides.',
    'fields' => array(
      'id' => array(
        'description' => 'The primary identifier.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'type' => array(
        'description' => 'Carousel name.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'title' => array(
        'description' => 'Slide title.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => 'Slide description.',
        'type' => 'text',
      ),
      'weight' => array(
        'description' => 'The weight of the slide in the carousel that it resides in',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'data' => array(
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'big',
        'serialize' => TRUE,
        'description' => 'A serialized array of additional data related to this model type.',
      ),
    ),
    'primary key' => array('id'),
  );

  // Table for storing data of entity exportable bundles.
  $schema['carousel_manager_type'] = array(
    'description' => 'The base table for Carousel Manager carousels.',
    'fields' => array(
      'id' => array(
        'description' => 'The primary identifier.',
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'type' => array(
        'description' => 'Carousel machine name.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'label' => array(
        'description' => 'Human-readable name of the carousel.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => 'A brief description of the carousel.',
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'medium',
        'translatable' => TRUE,
      ),
      'data' => array(
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'big',
        'serialize' => TRUE,
        'description' => 'A serialized array of additional data related to this model type.',
      ),
    ) + entity_exportable_schema_fields(),
    'primary key' => array('id'),
    'unique keys' => array(
      'type' => array('type'),
    ),
  );

  return $schema;
}

/**
 * Implements hook_disable().
 */
function carousel_manager_uninstall() {

  $background = field_read_instances(
    array(
      'field_name' => 'carousel_manager_background',
      'entity_type' => 'carousel_manager'
    ), array(
      'include_inactive' => TRUE,
      'include_deleted' => TRUE
    )
  );

  foreach ($background as $instance) {
    field_delete_instance($instance);
    field_purge_instance($instance);
  }

  $main_image = field_read_instances(
    array(
      'field_name' => 'carousel_manager_main_image',
      'entity_type' => 'carousel_manager'
    ), array(
      'include_inactive' => TRUE,
      'include_deleted' => TRUE
    )
  );

  foreach ($main_image as $instance) {
    field_delete_instance($instance);
    field_purge_instance($instance);
  }

  field_purge_batch(1);
  drupal_flush_all_caches();
}
