<?php

/**
 * @file
 * Default theme implementation for the Slick carousel template.
 *
 * Available variables:
 * - $items: The array of items containing main image/video/audio, and optional
 *     image/video/audio overlay and captions.
 * - $settings: A cherry-picked settings that mostly defines the slide HTML or
 *     layout, and none of JS settings/options which are defined at data-slick.
 * - $attributes: The array of attributes to hold the container classes, and id.
 * - $content_attributes: The array of attributes to hold the slick-slider and
 *     data-slick containing JSON object aka JS settings the Slick expects to
 *     override default options. We don't store these JS settings in the normal
 *     <head>, but inline within data-slick attribute instead.
 */
dpm($slides);
?>
<section class="slick-wrapper">
  <div>
    <div class="slide-background">
      <img src="<?php echo image_style_url('carousel_manager_background', $slides[0]['content']['background image']) ?>" />
    </div>
    <div class="slide-content">
      <div class="slide-text-wrapper">
        <div class="slide-text">
          <h3><?php echo $slides[0]['title']; ?></h3>
          <p><?php echo $slides[0]['content']['body']; ?></p>
        </div>
      </div>
      <div class="slide-main-image">
        <img src="<?php echo image_style_url('carousel_manager_main', $slides[0]['content']['main image']) ?>" />
      </div>
    </div>
  </div>
</section>

