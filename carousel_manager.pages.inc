<?php

/**
 * @file
 * Callbacks for viewing entities.
 */

/**
 * Entity view callback.
 *
 * @param object $entity
 *   Entity to view.
 *
 * @return array
 *   Renderable array.
 */
function carousel_manager_view($entity) {
  drupal_set_title(entity_label('carousel_manager', $entity));
  // Return automatically generated view page.
  return entity_view('carousel_manager', array(entity_id('carousel_manager', $entity) => $entity));
}
