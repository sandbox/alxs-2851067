<?php
/**
 * @file
 * Block template for carousel manager carousels.
 */
?>
<div class="content"<?php print $content_attributes; ?>>
  <?php print render($content); ?>
</div>
